class User < ActiveRecord::Base  
  #include BCrypt  
  attr_accessible :active, :name, :password, :username, :password_confirmation
  attr_accessor :password_confirmation
  
  before_save do |user|
    user.username.downcase!
    user.active = true
    user.password =  Digest::MD5.hexdigest(password)
  end

  validates :name, presence: true
  validates :username, presence: true, 
                       uniqueness: {case_sensitive: false}
  validates :password, presence: true, length: {minimum: 6}
  validates :password_authentication, acceptance: {accept: true}

  def password_authentication
    self.password == self.password_confirmation
  end

end
