class PortalController < ApplicationController
  include ApplicationHelper
  
  before_filter except: [:index, :login, :access_denied] do
    redirect_to access_denied_path unless authenticated?
  end

  before_filter only: [:index] do
    redirect_to panel_path if authenticated?
  end

  def index
    @user = User.new
  end

  def login
    @user = User.new(params[:user])

    respond_to do |format|
      if authentication(@user)
        format.html {redirect_to panel_path }
      else
        format.html {redirect_to root_path, notice: 'Login fail!' }
      end
    end
  end

  def end_session
    logout
    redirect_to root_path
  end

  def panel
  end

  def access_denied
  end

end
