module ApplicationHelper

  def authentication(current_user)
    user = User.find_by_username(current_user.username.downcase)
    unless user.nil?
      if Digest::MD5.hexdigest(current_user.password) == user.password
        session[:user] = current_user
      end
    else
      return false
    end  
  end

  def logout
    session[:user] = nil
  end

  def authenticated?
    session[:user]
  end

end
